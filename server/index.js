const { GraphQLServer } = require('graphql-yoga');
const { createWriteStream, unlink } = require('fs');
const Storage = require('@google-cloud/storage');
const opn = require('opn');

//Instatiate G-Storage, to enable public view of the image
const storage = Storage({
    projectId: '****'
})

//Cloud Storage Bucket name
const bucket = '****';

let filePath = "";
let fileURL = '';

/**
 * GraphQL type definition
 */
const typeDefs = `
    scalar Upload

    type Mutation {
        uploadFile(file: Upload!): Boolean
    }

    type Query {
        hello: String
    }
`;

/**
 * Uploading to google cloud storage
 * @param {*} bucketName 
 * @param {*} filename 
 */
function uploadFile(bucketName, filename) {
    storage
      .bucket(bucketName)
      .upload(filename, {
        gzip: true,
      })
      .then(() => {
        console.log(`${filename} uploaded to ${bucketName}.`);
        //Deleting the file from local machine
        unlink(filename, (err) => {
            if (err) {
                throw err;
            }
            console.log(`File deleted`);
        });
      })
      .catch(err => {
        console.error(`ERROR: Something is wrong`);
      });
}

/**
 * Function that resolves the file from the frontend and saves it
 * on the local machine for upload to the cloud
 */
const storeUpload = ({ stream, filename }) =>
    new Promise((resolve, reject) => {
        stream
            .pipe(filePath = createWriteStream(filename))
            .on("finsih", () => {
                console.log(`${filePath} Hello`);
                resolve();
            })
            .on("error", reject);
            //concatting the absolute filePath
            filePathAbs = `./${filePath.path}`;
            //Uploading the file to g-cloud
            uploadFile(bucket, filePathAbs);
            fileURL = `https://storage.googleapis.com/${bucket}/${filePath.path}`;
            //Open the new file on a new tab in your default browser
            opn(fileURL);
    });

//GraphQL resolver
const resolvers = {
    Mutation: {
        uploadFile: async (parent, { file }) => {
            const { stream, filename } = await file;
            await storeUpload({ stream, filename });
            return true;
        }
    },
    Query: {
        hello: () => "Go to bed"
    }
};

//Since graphql-yoga is being used, it can handle server deployment
//Server startup defaults to port 4000.
const server = new GraphQLServer({ typeDefs, resolvers });
server.start(() => console.log(`Server running on port 4000`));